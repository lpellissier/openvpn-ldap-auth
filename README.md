# openvpn-ldap-auth

**openvpn-ldap-auth** is a Python authentication script called by OpenVPN server to authenticate and authorize users with LDAP.

It's features 

- Authentication is done by connecting to an LDAP server.
- Authorization of users allowed to connect to OpenVPN server is defined in an LDAP group with an LDAP filter expression.
- Can authorize all authenticated users or only members of an LDAP group.
- Credentials can be passed `by-file` or `by-env` (parameter of `auth-user-pass-verify`). The script automatically detects which transfert method OpenVPN uses.
- For some users it can force user's certificate `commonName` to match login name. These users must be member of a distinct LDAP group. These users can't borrow certificate of another user. This imply credentials passed `by-env`. This may be useful for users with higher privileges.
- For all users it can force user's certificate `commonName` to match login name.


# OpenVPN configuration

To use this script you just need to configure your OpenVPN `server.conf` file. You just need to choose whether credentials will be passed `by-file` or `by-env`.

With `by-file` method OpenVPN will create a temporary file with two lines : username and password. The full path of the file will be passed as command line argument. Limitation of this mode implies that the `commonName` of the user's certificate won't be available to the script. So it is not capable of checking if the certificate belongs to the user or if it has borrowed it from another user. 


```
script-security 2
auth-user-pass-verify scripts/openvpn-ldap-auth.py via-file
```

With `by-env` method OpenVPN will transmit credentials with environment viariables. This is the only method (as of OpenVPN 2.4) to check if the `commonName` certificate matches the username (login).

```
script-security 3
auth-user-pass-verify scripts/openvpn-ldap-auth.py via-env
```
  - Import a HTML file and watch it magically convert to Markdown
  - Drag and drop images (requires your Dropbox account be linked)


Then you need to configure the Python script to your needs. Edit variables at the top of the file.

On the client side put these in your client.ovpn file :


```
auth-user-pass
username-as-common-name
duplicate-cn

```


# Configuration

You configure the script by editing few lines at the top of the script :

* `LDAPServer` : URL of your LDAP server
* `baseDN` : Your base DN
* `userFilter` : An anonymous LDAP bind is first made to the LDAP server. User's DN must be searched. This is LDAP filter used to look up user's DN 
* `groupDN` : Based DN to search LDAP group filters
* `groupFilter` : LDAP group filter to check if a user is authorized to connect to the OpenVPN server. In this LDAP group filter `{dn}` will be replaced by the full DN of found users.
* `certGroupFilter` : Same as `groupFilter` but these users must have the same certificate `commonName` as their username.
* `AuthorizeAll` : If set to `True` all authenticated users are allowed to connect. `groupFilter` and `certGroupFilter` are ignored.
* `DEBUG` : You can set ip to `True` if you want a verbose logging or `False` if a minimum output is enough.


# LDAP Authorization

If you choose to allow OpenVPN connexion only to selected users. You must set `AuthorizeAll` to `False` and edit `groupFilter`LDAP group filter :

```
groupDN = "ou=groups,dc=always,dc=paris"
groupFilter = "(&(objectClass=groupOfUniqueNames)(cn=openvpn)(uniqueMember={dn}))"
```

In your LDAP server you can add the group definition :

```
dn: cn=openvpn,ou=groups,dc=always,dc=paris
objectClass: top
objectClass: groupOfUniqueNames
cn: openvpn
uniqueMember: uid=mylogin,ou=citizens,dc=always,dc=paris

dn: ou=citizens,dc=always,dc=paris
objectClass: posixAccount
objectClass: person
objectClass: account
objectClass: inetOrgPerson
objectClass: top
uid=mylogin,ou=citizens,dc=always,dc=paris
...
```


# Test

## `by-file`

To test your LDAP search filter it is convenient to do it manually. You just need to create a two lines file and turn on debugging (`DEBUG = True`):

```
$ cat test.txt
mylogin
mypassword

$  ./ldap-auth.py test.txt
openvpn-ldap-auth.py Reading credentials from file test.txt
openvpn-ldap-auth.py username is mylogin
openvpn-ldap-auth.py Searching (&(objectClass=posixAccount)(uid=mylogin))
openvpn-ldap-auth.py Found uid=mylogin,ou=personnel-ales,dc=always,dc=paris
openvpn-ldap-auth.py Connecting LDAP as uid=mylogin,ou=citizens,dc=always,dc=paris
openvpn-ldap-auth.py Valid credentials for user mylogin
openvpn-ldap-auth.py Connected as uid=mylogin,ou=citizens,dc=always,dc=paris
openvpn-ldap-auth.py Searching VPN group (&(objectClass=groupOfUniqueNames)(cn=openvpn)(uniqueMember=uid=mylogin,ou=citizens,dc=always,dc=paris))
openvpn-ldap-auth.py User mylogin not found in VPN group
openvpn-ldap-auth.py User mylogin rejected
```

## `by-env`

And to test it with credentials transitted by environment variables you can edit a script :

```
# cat test.sh
#!/bin/bash

export username="mylogin"
export password="mypassword"
export common_name="mylogin"

./ldap-auth.py

unset username password common_name

# ./test.sh 
openvpn-ldap-auth.py Reading credentials from environment
openvpn-ldap-auth.py username is mylogin, common_name is mylogin
openvpn-ldap-auth.py Searching (&(objectClass=posixAccount)(uid=mylogin))
openvpn-ldap-auth.py Found uid=mylogin,ou=citizens,dc=always,dc=paris
openvpn-ldap-auth.py Connecting LDAP as uid=mylogin,ou=citizens,dc=always,dc=paris
openvpn-ldap-auth.py Valid credentials for user mylogin
openvpn-ldap-auth.py Connected as uid=mylogin,ou=citizens,dc=always,dc=paris
openvpn-ldap-auth.py Searching VPN cert group (&(objectClass=groupOfUniqueNames)(cn=openvpn-admins)(uniqueMember=uid=mylogin,ou=citizens,dc=always,dc=paris))
openvpn-ldap-auth.py User mylogin found in VPN cert group
openvpn-ldap-auth.py username and cert's common_name are the same : mylogin
openvpn-ldap-auth.py User mylogin authorized
```



