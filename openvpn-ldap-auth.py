#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""

BSD 2-Clause License

Copyright (c) 2019, Laurent PELLISSIER
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

  * Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.

      THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
      AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
      IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
      DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
      FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
      DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
      SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
      CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
      OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
      OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

-------

    Author: Laurent.Pellissier@mines-ales.fr
    Date  : 12/2019
    github: https://github.com/lpellissier


"""

import os
import sys
import ldap

#-------------------- Configuration ----------------------
LDAPServer = "ldap://127.0.0.1:389"
baseDN = "dc=always,dc=paris"

# LDAP filter to search the DN of username 
userFilter = "(&(objectClass=posixAccount)(uid={username}))"

# Base DN for LDAP group filter searches
groupDN = "ou=groups,dc=always,dc=paris"

# LDAP filter to check if DN is allowed to connect to OpenVPN
# Group filer search will be relative to groupDN.
groupFilter = "(&(objectClass=groupOfUniqueNames)(cn=openvpn)(uniqueMember={dn}))"

# LDAP filter to check if DN is allowed to connect to OpenVPN. For this DN certificate's
# commonName must be the same of the login sent to OpenVPN Server.
# Group filer search will be relative to groupDN.
certGroupFilter = "(&(objectClass=groupOfUniqueNames)(cn=openvpn-admins)(uniqueMember={dn}))"

bindDN = ""

# If True all authenticated users are allowed to connect. groupFilter and
# certGroupFilter are ignored.
AuthorizeAll = False

# Are username and Certificate's commonName must be the same ?
# Disabled if credentials are send via-file
checkCommonName = False


# Set full verbosity of logs
DEBUG = True
#DEBUG = False

# ----------------------- End of parameters ---------------------------


#----------------------------------
# Read username/password from text file filename. Values are returned in global
# variables username, password
# Returns True if success, False else
def readIdFromFile(filename):
  global username, password, commonName
  try:
    fo = open(filename, "r")
    username = fo.readline()
    password = fo.readline()
    # Remove CR
    username = username.replace('\n', '')
    password = password.replace('\n', '')
    commonName = ""
    if DEBUG: myLog("username is "+username)
  except IOError as e:
    myLog(str(e))
    return False
  finally:
    fo.close()
  return True


#----------------------------------
# Read username/password/Cert common_name from environment. Values are returned in
# globals variables username, password, commonName
# Returns True if success, False else
def readIdFromEnv():
  global username, password, commonName
  try:
    username = os.environ['username']
    password = os.environ['password']
    commonName = os.environ['common_name']
  except KeyError as e:
    myLog("environment variable "+str(e)+" is missing")
    return False
  if DEBUG:
    myLog("username is "+username+", common_name is "+commonName)
  return True


#----------------------------------
# Log msg prefixed by filename
def myLog(msg):
  print(os.path.basename(__file__)+" "+msg)


#----------------------------------
# Check is Cert's commonName is the same as username provided by user. If both
# are different then certificate doesn't belong to user. It may be stolen.
# Access is denied
# The test can only be applied if credentials are passed in environment.
def checkCertCommonName(username, commonname):
  if len(commonName) == 0 or not checkCommonName:
    if DEBUG: myLog("Cert's commonName checking disabled by config")
    return
  # Checks if the auth user is using the correct client certificate
  if username != commonname:
    myLog("Access denied. Username and Cert's commonName do not match")
    sys.exit(1)
  else:
    myLog("Username and Cert's commonName match")


#----------------------------------
# Check if username is member of allowed VPN group.
# Returns True if username is autorized to connect to VPN
def isAuthorized(con, username):
  attrs = ['uniqueMember']
  searchFilter = groupFilter.format(dn=bindDN)
  if DEBUG: myLog("Searching VPN group "+searchFilter)
  results = con.search_s(groupDN, ldap.SCOPE_SUBTREE, searchFilter, attrs)
  del con
  if len(results) == 1:
    myLog("User "+username+" found in VPN group")
    myLog("User "+username+" authorized")
    return True
  else:
    myLog("User "+username+" not found in VPN group")
    myLog("User "+username+" rejected")
    sys.exit(1)
  return False


#----------------------------------
# Check if username is member of allowed VPN group.
# Returns True if username is autorized to connect to VPN
def isCertAuthorized(con, username):
  global commonName

  attrs = ['uniqueMember']
  searchFilter = certGroupFilter.format(dn=bindDN)
  if DEBUG: myLog("Searching VPN cert group "+searchFilter)
  results = con.search_s(groupDN, ldap.SCOPE_SUBTREE, searchFilter, attrs)
  del con
  if len(results) == 1:
    myLog("User "+username+" found in VPN cert group")
    if username.lower() == commonName.lower():
      if DEBUG:
        myLog("username and cert's common_name are the same : "+username)
      myLog("User "+username+" authorized")
      return True
    else:
      myLog("login "+username+" and certificate's commonName "+commonName+" are different. Access denied")
  else:
    myLog("User "+username+" rejected")
    sys.exit(1)
  return False


####------- Start -------####

# Try to guess whether arguments are passed by-file or by-env
# If arguments are passed by-env thare are two args and the second is ""
if len(sys.argv) == 2 and len(sys.argv[1]) > 0:
  if DEBUG: myLog("Reading credentials from file "+sys.argv[1])
  if not readIdFromFile(sys.argv[1]):
    myLog("Error getting credentials. Access denied")
    sys.exit(1)
else:
  if DEBUG: myLog("Reading credentials from environment")
  if not readIdFromEnv():
    myLog("Error getting credentials. Access denied")
    sys.exit(1)

checkCertCommonName(username, commonName)

# Connects to the LDAP server, search the DN for uid=username
#ldap.set_option(ldap.OPT_X_TLS_REQUIRE_CERT, ldap.OPT_X_TLS_ALLOW)
con = ldap.initialize(LDAPServer)
con.protocol_version = ldap.VERSION3

try:
  # LDAP anonymous bind to search the DN's username to connect as
  con.simple_bind_s()
  attrs = ['uid']
  searchFilter = userFilter.format(username=username)
  if DEBUG: myLog("Searching "+searchFilter)
  dn = con.search_s(baseDN, ldap.SCOPE_SUBTREE, searchFilter, attrs )
  if len(dn) != 1:
    if DEBUG: myLog("Not found "+searchFilter)
    else:
      myLog(username+"not found in LDAP. Access denied")
    sys.exit(1)

  bindDN = dn[0][0]        # get DN to connect as
  if DEBUG:
    myLog("Found "+bindDN)
    myLog("Connecting LDAP as "+bindDN)

  # LDAP authenticated connexion to check user's password
  con.simple_bind_s(bindDN, password)
  myLog("Valid credentials for user "+username)
  if DEBUG: myLog("Connected as "+bindDN)
except ldap.LDAPError as e:
  myLog(username+" access denied")
  del con
  sys.exit(1)

if AuthorizeAll:
  if DEBUG: myLog("All authenticated users are authorized")
  del con
  sys.exit(0)

if len(commonName) > 0:
  if isCertAuthorized(con, username):
    del con
    sys.exit(0)
elif isAuthorized(con, username):
  del con
  sys.exit(0)


# If we came until here connexion must be denied
del con
sys.exit(1)

